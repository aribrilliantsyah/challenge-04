const express = require("express");
const UserGameRouter = require("./routes/UserGameRouter");
const UserBiodataRouter = require("./routes/UserBiodataRouter");
const UserHistoryRouter = require("./routes/UserHistoryRouter");
const app = express()
const port = 3000;

app.use(express.json())
app.use(UserGameRouter)
app.use(UserBiodataRouter)
app.use(UserHistoryRouter)

app.listen(port, () => {
    console.log(`Running at port ${port}`)
})