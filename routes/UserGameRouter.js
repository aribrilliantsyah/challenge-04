const express = require('express');
const router = express.Router()
const UserGameController = require('../controllers/UserGameController');
const userGameCtl = new UserGameController();
const group = '/api/v1';

router.get(`${group}/user-game/`, userGameCtl.getAll)
router.get(`${group}/user-game/:id`, userGameCtl.findByID)
router.post(`${group}/user-game`, userGameCtl.create)
router.put(`${group}/user-game/:id`, userGameCtl.update)
router.delete(`${group}/user-game/:id`, userGameCtl.delete)

module.exports = router