const express = require('express');
const router = express.Router()
const UserBiodataController = require('../controllers/UserBiodataController');
const userBiodataCtl = new UserBiodataController();
const group = '/api/v1';

router.get(`${group}/user-biodata/`, userBiodataCtl.getAll)
router.get(`${group}/user-biodata/:id`, userBiodataCtl.findByID)
router.post(`${group}/user-biodata`, userBiodataCtl.create)
router.put(`${group}/user-biodata/:id`, userBiodataCtl.update)
router.delete(`${group}/user-biodata/:id`, userBiodataCtl.delete)

module.exports = router