const express = require('express');
const router = express.Router()
const UserHistoryController = require('../controllers/UserHistoryController');
const userHistoryCtl = new UserHistoryController();
const group = '/api/v1';

router.get(`${group}/user-history/`, userHistoryCtl.getAll)
router.get(`${group}/user-history/:id`, userHistoryCtl.findByID)
router.post(`${group}/user-history`, userHistoryCtl.create)
router.put(`${group}/user-history/:id`, userHistoryCtl.update)
router.delete(`${group}/user-history/:id`, userHistoryCtl.delete)

module.exports = router